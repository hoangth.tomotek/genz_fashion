import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { ShoppingCartService } from 'src/app/services/shopping-cart.service';
import { ConfirmOrderComponent } from '../confirm-order/confirm-order.component';


@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.scss']
})
export class CheckoutComponent implements OnInit {

  fullname: string = '';
  phone: string = '';
  address: string = '';
  paymentMethod: string = '';
  notes: string = '';
  productDetails: any[] = [];
  total: number = 0;

  constructor(
    private router: Router,
    private dialog: MatDialog,
    private snackBar: MatSnackBar,
    private shoppingService: ShoppingCartService
  ) {
    const navigation = this.router.getCurrentNavigation();
    if (navigation && navigation.extras && navigation.extras.state) {
      this.productDetails = navigation.extras.state.products;
      this.total = navigation.extras.state.total;
    } else {
      this.router.navigate(['/cart']);
    }
   }

  ngOnInit(): void {
  }

  placeOrder() {
    const dialogRef = this.dialog.open(ConfirmOrderComponent, {
      width: '400px',
      data: { fullname: this.fullname, phone: this.phone, address: this.address, paymentMethod: this.paymentMethod, notes: this.notes, productDetails: this.productDetails, total: this.total }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        const orderData = { fullname: this.fullname, phone: this.phone, address: this.address, paymentMethod: this.paymentMethod, notes: this.notes, productDetails: this.productDetails, total: this.total };
        this.shoppingService.placeOrder(orderData).subscribe(response => {
          this.snackBar.open('Đặt hàng thành công!', 'Đóng', { duration: 3000 });
        }, error => {
          console.error('Đặt hàng thất bại:', error);
          this.snackBar.open('Đặt hàng thất bại. Vui lòng thử lại sau.', 'Đóng', { duration: 3000 });
        });
      }
    });
  }
}
